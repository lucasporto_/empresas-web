import React, { Component } from "react";

import { Link, withRouter, Redirect } from "react-router-dom";



import iApi from './iApi'

  
class Auth extends Component {
  state = {
    email: '',
    password: '',
    redirect: false,
    
  };



  handleLogin = async e => {
    e.preventDefault();
    const token = localStorage.getItem['access-token'];
    const uid = localStorage.getItem.uid;
    const client = localStorage.getItem.client;

    
     const response = iApi.get('/userapi/v1/enterprises/1', {
         params: {
         'acess-token': token,
         uid: uid,
         client: client
         }
       })
       .then(function (response) {
         console.log(response);
       })
       .catch(function (error) {
         console.log(error);
       })
       .finally(function () {
         // always executed
       });  
       this.setState(response);
        
  };

  
    render() {
      return (
        <div className="container">
          <form className="form" onSubmit={this.handleLogin}>
          {this.state.error && <p>{this.state.error}</p>}
            <h1>BEM VINDO AO EMPRESAS</h1>
            <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            <div className="row">
            <input id="email"
              type="email"
              placeholder="E-mail"
              onChange={e => this.setState({ email: e.target.value })}
            />
            <input id="senha"
              type="password"
              placeholder="Senha"
              onChange={e => this.setState({ password: e.target.value })}
            />
            </div>
            <div className="button-fixo">
            <button onClick={e => this.setState({ redirect: true })} type="submit">ENTRAR</button>
            </div>
          </form>
        </div>
      );
    }
  }
  
  export default withRouter(Auth);
