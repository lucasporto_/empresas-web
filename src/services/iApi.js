import axios from "axios";
// import { getToken } from "./auth";

const iApi = axios.create({
  baseURL: "http://empresas.ioasys.com.br/"
});

// iApi.interceptors.request.use(async config => {
//   const token = getToken();
//   if (token) {
//     config.headers.Authorization = `Bearer ${token}`;
//   }
//   return config;
// });

export default iApi;