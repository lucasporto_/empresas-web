import React, { Component } from "react";
import api from "../../services/api";
import "./style.css";
import { Link } from 'react-router-dom';

export default class Main extends Component {
    
    state = {
        products: [],
        productInfo: {},
        page: 1,
    };
    
    componentDidMount() {
        this.loadProducts();
    }


loadProducts = async (page = 1) => {
    // response guarda as info de /products?{page}
    const response = await api.get(`/products?page=${page}`);

    // cria as variáveis docs e product.info
    // docs retorna os produtos da API
    // products.Info retorna o RESTO das info

    const { docs, ...productInfo } = response.data;

    // atualiza as variáveis do State
    this.setState({ products: docs, productInfo, page});
};

prevPage = () => {

    // page é a página atual
    // productInfo é onde está a propriedade pages
    const { page, productInfo} = this.state;

    // productInfo.pages retorna a quantidade de páginas
    // para ver se estamos na última página
    if (page === 1) return;

    // se não, subtrai a página atual - 1
    const pageNumber = page - 1;

    this.loadProducts(pageNumber);
   
}

nextPage = () => {

    // page é a página atual
    // productInfo é onde está a propriedade pages
    const { page, productInfo} = this.state;

    // productInfo.pages retorna a quantidade de páginas
    // para ver se estamos na última página
    if (page === productInfo.pages) return;

    // se não, soma a página atual + 1
    const pageNumber = page + 1;

    this.loadProducts(pageNumber);
}



render () {
    return (
        <div className="product-list">
            { this.state.products.map(product => (
                <article key={product._id}>
                <strong>{product.title}</strong>
                <p>{product.description}</p>

                <Link to={`/products/${product._id}`}>Acessar</Link>
                
                
                </article>
            ))}
            
            <div className="actions">
                <button disabled={ this.state.page === 1 } 
                onClick={this.prevPage}>Anterior</button>

                <button disabled={ this.state.page === this.state.productInfo.pages } 
                onClick={this.nextPage}>Próximo</button>
            </div>

        </div>

    );
}
}
