import React, { Component } from "react";

import { Link, withRouter, Redirect } from "react-router-dom";

import Logo from "../../assets/logoHome.png"

import "./style.css"

import iApi from '../../services/iApi';

  
class Login extends Component {
  state = {
    email: '',
    password: '',
    redirect: false,
    
  };



  handleLogin = async e => {
    e.preventDefault();
        const { email, password, redirect } = this.state;
        //console.log(email, password);

        const response = await iApi.post("api/v1/users/auth/sign_in/", { email, password })
        .then(function (response) {
          const client = response.headers.client;
          const tokenClient = response.headers['access-token'];
          const uid = response.headers.uid;
          console.log(uid);
          localStorage.setItem('access-token', tokenClient);
          localStorage.setItem('uid', uid);
          localStorage.setItem('client', client);
        })
        .catch(function (error) {
          console.log(error);
        });

        if (localStorage.getItem['access-token'] !== null && redirect === true) {
          this.props.history.push('/home');
        } else {
          this.props.history.push('/');
       }
        
  };



  
    render() {
      return (
        <div className="container">
          <form className="form" onSubmit={this.handleLogin}>
          {this.state.error && <p>{this.state.error}</p>}
            <img src={Logo} alt="Ioasys logo" />
            <h1>BEM VINDO AO EMPRESAS</h1>
            <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            <div className="row">
            <input id="email"
              type="email"
              placeholder="E-mail"
              onChange={e => this.setState({ email: e.target.value })}
            />
            <input id="senha"
              type="password"
              placeholder="Senha"
              onChange={e => this.setState({ password: e.target.value })}
            />
            </div>
            <div className="button-fixo">
            <button onClick={e => this.setState({ redirect: true })} type="submit">ENTRAR</button>
            </div>
          </form>
        </div>
      );
    }
  }
  
  export default withRouter(Login);
