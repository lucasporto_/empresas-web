import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 63vh;
  background: rgb(235 233 215);
  h1 {
    text-align: center;
    font-size: 18px;
    margin-bottom: 19px;

  }
`;

export const Row = styled.div`
  margin-top: 40px;

}
`;

export const Form = styled.form`
  width: 490px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 40px;
  img {
    width: 40%;
    margin: 10px 0 40px;
  }
  p {
    color: rgba(56,55,67, 0.9);
    margin-bottom: 15px;
    padding: 10px;
    width: 100%;
    text-align: center;
  }
  input {
    flex: 1;
    height: 46px;
    margin-bottom: 15px;
    padding: 5px 20px;
    color: #777;
    font-size: 20px;
    width: 100%;
    border: 0;
    border-bottom: 1px solid rgba(56,55,67, 0.5);
    background: rgb(235 233 215);
    &::placeholder {
      color: rgba(56,55,67, 0.5);
      
    }
    input#email {
      background-image: url(src/pages/login/email.png);
    }
  }

  `;

  export const ButtonFixo = styled.div`
 position: fixed;
 background: rgb(87,187,188);
bottom: 25px;
padding: 24px 130px;
border-radius: 5px;


  button {
    color: #fff;
    font-size: 16px;
    height: 56px;
    border: 0;
    cursor: pointer;
    width: 100%;

  }
  a {
    font-size: 16;
    font-weight: bold;
    color: #FFF;
    text-decoration: none;
    cursor: pointer;
  }
`;