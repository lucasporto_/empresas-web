import React from 'react';

import { BrowserRouter, Switch, Route, } from 'react-router-dom';

import Main from './pages/main';
import Product from './pages/products';
import Login from './pages/login/';
import Home from './pages/home';
import Teste from './services/auth'
// import { isAuthenticated } from './services/auth';



// O component Switch permite a randerização
// de apenas um component por rota
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/home" component={Home} />
            <Route exact path="/main" component={Main} />
            <Route path="/products/:id" component={Product} />
            <Route path='./services/auth' component={Teste} />
        </Switch>
    </BrowserRouter>

);

export default Routes;

